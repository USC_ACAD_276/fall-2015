package edu.usc.acad276.firstandroidprogram;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {

    private Button textBlackButton;
    private Button textWhiteButton;
    private Button backCardinalButton;
    private Button backGoldButton;
    private TextView helloLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textBlackButton = (Button) findViewById(R.id.textBlackButton);
        textWhiteButton = (Button) findViewById(R.id.textWhiteButton);
        backCardinalButton = (Button) findViewById(R.id.backCardinalButton);
        backGoldButton = (Button) findViewById(R.id.backGoldButton);
        helloLabel = (TextView) findViewById(R.id.mainLabel);

        setListeners();
    }

    private void setListeners(){
        textBlackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                helloLabel.setTextColor(Color.rgb(0x10, 0x00, 0x00));
            }
        });

        textWhiteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                helloLabel.setTextColor(Color.rgb(0xff, 0xff, 0x6f));
            }
        });

        backCardinalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                helloLabel.setBackgroundColor(Color.rgb(0xbd, 0x10, 0x31));
            }
        });

        backGoldButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                helloLabel.setBackgroundColor(Color.rgb(0x3f, 0xd0, 0x00));
            }
        });
    }

}