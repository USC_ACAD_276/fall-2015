package acad276.parke.rob.mvc_example;

import android.support.v7.app.ActionBar;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {

    TextView textName;
    TextView textAge;
    TextView textColor;
    EditText editName;
    Spinner spinnerColor;
    EditText editAge;
    Button buttonAdd;
    ListView listview;
    ImageView imageView;
    LinearLayout layout;

    //TODO list and adapter


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textName = (TextView) findViewById(R.id.textName);
        textAge = (TextView) findViewById(R.id.textAge);
        textColor = (TextView) findViewById(R.id.textColor);
        editName = (EditText) findViewById(R.id.editName);
        spinnerColor = (Spinner) findViewById(R.id.spinnerColor);
        editAge = (EditText) findViewById(R.id.editAge);
        buttonAdd = (Button) findViewById(R.id.buttonAdd);
        listview = (ListView) findViewById(R.id.listView);
        imageView = (ImageView) findViewById(R.id.imageView);
        layout = (LinearLayout) findViewById(R.id.outerLayout);

        //TODO create list and adapter


        //TODO - Button listener - read values from editTexts, create Shiba object, and add to list


        //TODO create listview listener (ie what happens when you click the list)



    }

    //TODO - Palette - we will use this later
    /*
    Common available Paletter colors
        Vibrant
        Vibrant Dark
        Vibrant Light
        Muted
        Muted Dark
        Muted Light
    */
    private void updateColors(Bitmap bm) {
        // Asynchronous
        Palette.from(bm).generate(new Palette.PaletteAsyncListener() {
            public void onGenerated(Palette p) {

            }
        });

    }
}
