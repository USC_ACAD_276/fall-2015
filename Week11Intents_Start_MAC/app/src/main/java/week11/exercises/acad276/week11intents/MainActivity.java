package week11.exercises.acad276.week11intents;

        import android.app.Activity;
        import android.os.Bundle;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.Spinner;
        import android.widget.Switch;

public class MainActivity extends Activity {

    private static final String TAG = MainActivity.class.getSimpleName();

    EditText editName;
    Spinner spinnerDifficulty;
    Switch switchCheatMode;
    Button buttonLaunchActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editName = (EditText)findViewById(R.id.editName);
        spinnerDifficulty = (Spinner)findViewById(R.id.spinnerLevel);
        switchCheatMode = (Switch)findViewById(R.id.switchCheatMode);
        buttonLaunchActivity = (Button)findViewById(R.id.button_launch_activity);



    }
}
